#include <iostream>
#include <fstream>
#include <sstream>
#include <random>
#include <math.h>
#include <fstream>


#include <base/solution.h>
#include <base/costFunction.h>
#include <init/init.h>
#include <base/fullNeighborEval.h>
#include <base/incrNeighborEval.h>
#include <algo/doubleIncr_biHC.h>
#include <algo/ils.h>
#include <algo/staticPerturb.h>
#include <algo/adaptivePertub.h>
#include <algo/rndPerturb.h>
#include <algo/xsearch.h>

using namespace std;

/*
  PILS

*/

int main(int argc, char ** argv){


  // file name of the instance file (json format)
  char * name_instance = argv[1];

  // random seed
  long unsigned int seed = atoi(argv[2]); 

  // perturbation strength
  string param_ils(argv[3]);

  // maximum number of iteration per hc
  unsigned nEvalHC = atoi(argv[4]);

  // maximum number of "evaluations" (incremental evals of perturbation + number of iteration of HC)
  unsigned nEvalMax = atoi(argv[5]);

  // output file name
  string output_name(argv[6]);

  // random generator
  std::mt19937 gen(seed);

  //Flux to write on stat
  ofstream statFlux(output_name);

  if(statFlux){
    cout << "reading file for stats " << endl;
  }else{
    cout << " Err: impossible to open file stats" << endl ;
  }

  // energy function
  std::cout << "read instance " << name_instance << " ..." << endl;
  CostFunction eval(name_instance);
  std::cout << "read instance done." << endl;
//  eval.print(); eval.printOnShort(std::cout);

  // random initialization
  Init init(gen, eval);
  IncrNeighborEval neighborEval(eval);

  // local search
  DoubleIncr_biHC hc(gen, eval, neighborEval, nEvalHC, std::cout);


  // perturbation
  Perturbation * perturbation;

  istringstream ss_ils(param_ils);
  unsigned perturb_id;
  ss_ils >> perturb_id;
  unsigned flatMax;
  switch (perturb_id) { 
  case 0: // static perturbation
    double s;
    unsigned strength;
    ss_ils >> s >> flatMax;

    if (s < 1) // proportion of the total length
      strength = round(s * eval.n_variables);
    else
      strength = (unsigned) s; 
    perturbation = new StaticPerturb(gen, eval, neighborEval, strength);
    break;

  case 1: // random perturbation
    unsigned strengthMin, strengthMax;
    ss_ils >> strengthMin >> strengthMax >> flatMax; 
    perturbation = new RandomPerturb(gen, eval, neighborEval, strengthMin, strengthMax);
    break;

  case 2: // adaptive perturbation
    double minStrength, maxStrength, incrFactor, decrFactor;
    ss_ils >> minStrength >> maxStrength >> incrFactor >> decrFactor >> flatMax;
    perturbation = new AdaptivePerturb(gen, eval, neighborEval, minStrength, maxStrength, incrFactor, decrFactor);
    break;
  }


  // CrossOVer algorithm search initialisation
  Xsearch xs(eval, neighborEval, hc, *perturbation, nEvalMax, flatMax, statFlux);


  //------------------------------------------------------
  // computation and output

  Solution x;

  // random initialization
  init(x);    
  eval(x);
  xs.nEval = 1;

  // do it
  xs(x);

  // print result
  eval(x);
  cout.precision(9);
  cout << x << endl;

  statFlux.close();

  return 1;

}
