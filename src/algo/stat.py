import os
from pylab import * 
import matplotlib.pyplot as plt
import numpy as numpy

statFile = open("../../build/stats.txt", 'r')
statData =statFile.read()


enP1=[]
enP2=[]
nEval=[]
nbCompCo=[]
nbIt=[]
It=0

for ligne in statFile:
	enP1.append( int(ligne.split[0]) )
	enP2.append( int(ligne.split[1]) )
	nEval.append( int(ligne.split[2]) )
	nbCompCo.append( int(ligne.split[3]) )
	nbIT.append(It)
	It+1

nbIt=linspace(0, len(enP1), len(enP1))

plt.figure()
plt.plot(enP1,nbIt,'r', label='parent 1')
plt.plot(enP2,nbIt,'b', label='parent 2')
plt.plot(nEval,nbIt,'g', label='number of evaluation')
plt.plot(nbCompCo,nbIt,'y', label='number of connexes components')
plt.legend()
plt.xlabel('number of iterations')

plt.show()

"""
plt.clf()
x=np.linspace(nbIter, 1000)


plt.show()
"""

stat.close()
