/*
  #ifndef __ils_h
  #define __ils_h
*/
#include <random>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <map>


#include <algo/localSearch.h>
#include <base/solution.h>
#include <base/costFunction.h>
#include <base/neighborEval.h>
#include <base/incrNeighborEval.h>
#include <algo/perturbation.h>
#include <base/Xover.h>
#include <init/init.h>


using namespace std; 

class Xsearch  : public LocalSearch {
public : 
  using LocalSearch::nEval;
  using LocalSearch::eval;
  using LocalSearch::id;

  Xsearch(CostFunction & _eval,
          IncrNeighborEval & _neighborEval,
          LocalSearch & _ls1,
          Perturbation & _pertu1,
          unsigned long long _nEvalMax,
          unsigned _flatMax,
          ostream & statFlux) : LocalSearch(_eval), neighborEval(_neighborEval), ls1(_ls1), pertu1(_pertu1), 
                                nEvalMax(_nEvalMax), flatMax(_flatMax), statF(statFlux) {

  }

  virtual void operator()(Solution & sol1){
    unsigned i=0;
    Xover xo(eval, statF); 
    pertu1.init(sol1);
    ls1.nEval = 0;
    ls1(sol1);
    nEval += ls1.nEval;
    unsigned flat = 0;
    while(nEval < nEvalMax && flat < flatMax){

      if ( abs(sol1.fitness() - lastFit) < 10e-6 ){
        flat++;
      } else {
        flat=0;
      }
      lastFit = sol1.fitness();
      sol2 = sol1;
      pertu1.nEval =0;
      pertu1(sol2);
      nEval += pertu1.nEval;

      ls1.nEval = 0;
      ls1(sol2);
      nEval += ls1.nEval;

      statF << sol1.fitness() << " " << sol2.fitness() << " " << nEval << " ";
      xo(sol1, sol2, sol1);	

      ls1.nEval = 0;
      ls1(sol1);
      nEval += ls1.nEval;


    }

  }


    	

protected :

  unsigned long long nEvalMax;

  unsigned flatMax;

  double lastFit = 0;

  bool same;

  bool growth;

  Solution sol2;

  ostream & statF;

  LocalSearch & ls1;

  Perturbation & pertu1;

  IncrNeighborEval & neighborEval;

  multimap<double, Solution> fitnesss;

  multimap<double, Solution>::iterator it1;

};
