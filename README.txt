Compile:

mkdir build
cd build
cmake ../src/exe
make


Execute: Assuming 2erw_0001.cfn is in data directory

./pils ../data/2erw_0001.cfn S "0 0.3 150" 500 60000 2erw-stats.txt >> 2erw-res.txt
S is an integer used as random seed
"0 0.3 150" are perturbation parameters: 0 means static perturbation, 0.3 is the size of the perturbation (proportional to sequence length) and 150 is the number of iterations allowed without improvement
500 is the maximum number of evaluations during each steepest descent 
60000 is the maximum total number of evaluations.
1aqt-stats.txt contains per iteration statistics about the run: fitness of both parent solutions, number of evaluations, number of connected components and whether the crossover was beneficial or not 

Our dataset is not included, feel free to contact us.
There is one sample instance in data subdirectory: unxz 2erw_0001.cfn.xz before running pils
You can generate your own instances using pompd :
https://forgemia.inra.fr/thomas.schiex/pompd

You will need to copy the cfn.gz file generated with pompd into your pils/data directory and gunzip it.

If you want to reconstruct a pdb structure (assuming the target is 2erw_0001):
1. paste the output solution (without the energy) into a file named 2erw_0001.gmec
2. copy that file back into your pompd/positive directory
3. run 'make 2erw_0001.opt'

Note: pompd uses Makefile and checks the order in which the targets were generated. The pdb (%.rlx) and %.resfile must be anterior to the %.cfn.gz file, which must be anterior to the %.gmec file.

Please contact us for troubleshooting.
